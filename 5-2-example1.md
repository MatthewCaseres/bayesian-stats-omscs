## What are we sampling from?

We are given that $`X|\theta \sim \text{N}(\theta,1)`$ and that $`\theta \sim \text{Ca}(0,1)`$. We cannot easily calculate the actual distribution because the integral in the denominator is a mess.

```math
\pi(\theta | x) = \frac{f(x|\theta)\pi(\theta)}{\int f(x|\theta)\pi(\theta) d\theta}
```

We settle for finding a distribution proportional to the desired distribution, then we sample using Metropolis-Hastings.

```math
\begin{align*}
\pi(\theta | x) &=  \frac{f(x|\theta)\pi(\theta)}{\int f(x|\theta)\pi(\theta) d\theta} \\ 
&\propto f(x|\theta)\pi(\theta)
\end{align*}
```

The relevant density functions are the Normal $`\text{N}(\theta,1)`$ and Cauchy $`\text{Ca}(0,1)`$ distributions. Our Normal distribution is proportional to $`e^{\frac{-1}{2}(x-\theta)^2}`$:

```math
f(x|\theta) = \frac{1}{\sigma \cdot \sqrt{2\pi}} e^{\frac{-1}{2}(\frac{x-\theta}{\sigma})^2} \text{ the normal distribution}  \\
= \frac{1}{\sqrt{2\pi}} e^{\frac{-1}{2}(x-\theta)^2} \text{ sigma is given to be 1}
\\
\propto e^{\frac{-1}{2}(x-\theta)^2} \text{ we only care about proportionality}
```

The Cauchy distribution is proportional to $`\frac{1}{1 + \theta^2}`$

```math
\text{Ca}(\theta_0, \gamma) = \frac{1}{\pi \gamma [1 + (\frac{\theta-\theta_0}{\gamma})^2]} \\
= \frac{1}{\pi (1 + \theta^2)} \text{ because } \theta_0=0,\gamma=1 \\
\propto \frac{1}{1 + \theta^2}
```

Putting this all together we see that 

```math
\pi(\theta|x) \propto f(x|\theta) \pi(\theta) \propto \frac{e^{\frac{-1}{2}(x-\theta)^2}}{1 + \theta^2}
```

So we are sampling from the posterior, but we don't have the normalized posterior, so we have to do this Monte Carlo thing.

## The Monte Carlo thing

In terms of the lectures, our **target** is $`\frac{e^{\frac{-1}{2}(x-\theta)^2}}{1 + \theta^2}`$. We need to find a markov chain with a stationary distribution that is equal to this underlying probability distribution. Without getting into the details too much, it is *known* that if we are currently at $`x`$ and we sample $`y`$ from some distribution $`q(y|x)`$ we can move to this proposed $`y`$ with probability $`\rho(x,y)`$ and stay in state $`x`$ with probability $`1-\rho(x,y)`$ and this will result in us sampling from the stationary distribution. $`\rho(x,y)`$ is chosen so that it satisfies the detailed balance equations.

```math
\rho(x,y) = \min(1,\frac{q(x|y)\pi(y)}{q(y|x)\pi(x)})
```

The distribution $`q(y|x)`$ is up to you. For symmetric kernel densities $`q(x|y) = q(y|x)`$ the kernel densities will cancel out when calculating $`\rho(x,y)`$.

```math
\rho(x,y) = \min(1,\frac{q(x|y)\pi(y)}{q(y|x)\pi(x)}) = \min(1,\frac{\pi(y)}{\pi(x)})
```

The distribution $`q(y|x)`$ need not depend on $`x`$ in which case it is just $`q(y)`$.

We can choose $`q(\theta '|\theta)`$ to be anything we want, as long as it is **admissable** (definition given in slides, and kind of confusing?). Anyhow, let's choose the Normal distribution $`N(2,1)`$ and see how it works.
```math
q(\theta '|\theta) = N(2,1) = \frac{1}{\sqrt{2\pi}}e^{-\frac{(\theta ' - 2)^2}{2}}
```


